

const express = require('express');

const app = express();

const port = 4000;


app.use(express.json());
	
app.use(express.urlencoded({extended: true}));

// mock database

let users = [
	{
		email: "nezukoKamado@gmail.com",
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "tanjiroKamado@gmail.com",
		username: "gonpanchiro",
		password: "iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	}

]

// GET Method
app.get('/home', (req, res) => {
	res.send('Hello Universe')
});


app.get('/users', (req, res) => {
	res.send(users)
});



app.listen(port, () => console.log(`The main Server is running at port ${port}`));